# Changelog

## v0.0.5

* Big improvement to file includes and use
* Messaging system is now more unified
* Can lex and parse (but not analyse) list comprehensions
* Attribute style indexing (e.g. `list.x`) now works
* Modifier characters now are understood
* Function-style echo and assert statements are now supported
* Updated allowed variable names
* Moved printing what files is being parsed into a `--verbose` mode

## v0.0.4

* Fixed bug where function calls were not added to used calls
* Separated out messages definition from main parsing code
* Recategorised messages
* Assign statements now work
* Let expressions (i.e. when used in an assignment) now work
* Added option to colourise the output

## v0.0.3

Fixed including the lark file in the wheel.

## v0.0.2

Improved file handling for include/use.

## v0.0.1

Basic analysis. Can parse most (all?) scad files. Messaging and file import rather *ad-hoc*/
