'''
This module contains openscad built-in defintions
'''

SCAD_VARS = ['PI', '$fa', '$fs', '$fn', '$t', '$vpr', '$vpt', '$vpd', '$children',
             '$preview']

SCAD_MODS = ['union', 'difference', 'intersection', 'echo', 'render', 'children',
             'assert', 'assign', 'circle', 'square', 'square', 'polygon', 'polygon',
             'text', 'import', 'projection', 'sphere', 'cube', 'cube', 'cylinder',
             'cylinder', 'polyhedron', 'import', 'linear_extrude', 'rotate_extrude',
             'surface', 'translate', 'rotate', 'rotate', 'scale', 'resize', 'mirror',
             'multmatrix', 'color', 'color', 'color', 'offset', 'hull', 'minkowski']

SCAD_FUNCS = ['is_undef', 'is_bool', 'is_num', 'is_string', 'is_list', 'concat', 'lookup',
              'str', 'chr', 'ord', 'search', 'version', 'version_num', 'parent_module',
              'abs', 'sign', 'sin', 'cos', 'tan', 'acos', 'asin', 'atan', 'atan2', 'floor',
              'round', 'ceil', 'ln', 'len', 'let', 'log', 'pow', 'sqrt', 'exp', 'rands',
              'min', 'max', 'norm', 'cross']
